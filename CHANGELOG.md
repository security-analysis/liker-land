# Changelog

## 2022-08-16
1b22109c5d61420d27992eb4dec1a17d3fc1df5d

### Added
- Users can view the number of an NFT they own on the details page.

## 2022-08-15
48995324301f648230b6cd02fed8daaa7f39db90

### Added
- New header UI for sign-in with various wallet extensions including Keplr, Keplr Mobile, Liker Land app, and Cosmostation.
- Users can go to My Page through the home page CTA.
