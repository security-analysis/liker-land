export const getAddress = state => state.address;
export const getSigner = state => state.signer;
export const getConnector = state => state.connector;
export const getLikerInfo = state => state.likerInfo;
